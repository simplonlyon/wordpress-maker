const express = require('express');
const mysql = require('mysql2/promise');
const fs = require('fs-extra');
const basicAuth = require('express-basic-auth');
const childProcess = require('child_process');

let wordpresses = [];
let lastRefresh = 0;

const app = express();
app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json({ extended: true }));

const sitesRoot = '/var/www/html';

const withAuth = () => basicAuth({ challenge: true, users: { 'admin': 'admin' } });

app.get('/wordpress-list', async (req, res) => {
    if (wordpresses.length > 0 && !req.query.refresh && new Date().getTime() - lastRefresh < 86400000) {
        res.json(wordpresses);
        return;
    }
    console.log('refreshing wordpress list');
    lastRefresh = new Date().getTime();
    wordpresses = [];
    const fabriques = await fs.readdir(sitesRoot);
    for (const fabrique of fabriques) {
        let isDir = (await fs.lstat(sitesRoot + '/' + fabrique)).isDirectory();
        if (isDir && !fabrique.match(/wordpress-maker|wp-.*|\.|\.\./)) {
            const promos = await fs.readdir(sitesRoot + '/' + fabrique);
            for (const promo of promos) {
                let isDir = (await fs.lstat(sitesRoot + '/' + fabrique + '/' + promo)).isDirectory();
                if (isDir && !promo.match(/\.|\.\./)) {
                    const sites = await fs.readdir(sitesRoot + '/' + fabrique + '/' + promo);
                    for (const site of sites) {
                        let isWordpress = fs.existsSync(sitesRoot + '/' + fabrique + '/' + promo + '/' + site + '/wp-config.php');
                        if (isWordpress) {
                            const wpConfig = await fs.readFile(sitesRoot + '/' + fabrique + '/' + promo + '/' + site + '/wp-config.php', { encoding: 'utf-8' });
                            const match = wpConfig.match(/define\( 'DB_NAME', '([a-zA-Z0-9_]+)' \);/);
                            if (match && match[1]) {

                                wordpresses.push({
                                    fabrique, promo, site, dbName: match[1]
                                });
                            }
                        }
                    }
                }
            }
        }
    }
    res.json(wordpresses);
});

app.post('/wordpress-remove', withAuth(), async (req, res) => {
    
    const connection = await mysql.createConnection({
        host: 'mysql',
        user: 'root',
        password: 'unbonmotdepasse',
        multipleStatements: true
    });
    let promises = [];
    for (const wordpress of req.body) {
        wordpresses = wordpresses.filter(item => wordpress.dbName != item.dbName);
        let isWordpress = (await fs.lstat(sitesRoot + '/' + wordpress.fabrique + '/' + wordpress.promo + '/' + wordpress.site + '/wp-config.php')).isFile();
        if (isWordpress) {
            promises.push(connection.query('DROP DATABASE '+wordpress.dbName));
            fs.rmdir(sitesRoot + '/' + wordpress.fabrique + '/' + wordpress.promo + '/' + wordpress.site)
        }
    }
    Promise.allSettled(promises).then(() => connection.destroy());
    
    res.end();
})

app.post('/wordpress-reset', withAuth(), async (req, res) => {
    
    const connection = await mysql.createConnection({
        host: 'mysql',
        user: 'root',
        password: 'unbonmotdepasse',
        multipleStatements: true
    });
    let promises = [];
    for (const wordpress of req.body) {
        let isWordpress = (await fs.lstat(sitesRoot + '/' + wordpress.fabrique + '/' + wordpress.promo + '/' + wordpress.site + '/wp-config.php')).isFile();
        if (isWordpress) {
            promises.push(connection.query('USE '+wordpress.dbName+'; UPDATE `wp_users` SET `user_login`="admin", `user_pass` = MD5( "simplon1234" ) WHERE id=1;'));
        }
    }
    Promise.allSettled(promises).then(() => connection.destroy());
    
    res.end();
})


app.post('/wordpress-no-plugin', withAuth(), async (req, res) => {
    
    const connection = await mysql.createConnection({
        host: 'mysql',
        user: 'root',
        password: 'unbonmotdepasse',
        multipleStatements: true
    });
    let promises = [];
    for (const wordpress of req.body) {
        let isWordpress = (await fs.lstat(sitesRoot + '/' + wordpress.fabrique + '/' + wordpress.promo + '/' + wordpress.site + '/wp-config.php')).isFile();
        if (isWordpress) {
            promises.push(connection.query('USE '+wordpress.dbName+'; UPDATE `wp_options` SET `option_value`="a:0:{}" WHERE `option_name`="active_plugins";'));
        }
    }
    Promise.allSettled(promises).then(() => connection.destroy());
    
    res.end();
})

app.post('/wordpress-generate', withAuth(), async (req, res) => {

    if (req.body.promo && req.body.fabrique && req.body.nb) {
        const promo = req.body.promo.replace(/[^A-Z0-9]/ig, "");
        const fabrique = req.body.fabrique.replace(/[^A-Z0-9]/ig, "");
        const starting = Number(req.body.starting)?Number(req.body.starting):1;
        const nb = Number(req.body.nb) + (starting-1);

        const path = `${sitesRoot}/${fabrique}/${promo}`;
        const config = fs.readFileSync('/wp-config.php', 'utf8');
        const dbSQL = fs.readFileSync('/db.sql', 'utf-8');
        const connection = await mysql.createConnection({
            host: 'mysql',
            user: 'root',
            password: 'unbonmotdepasse',
            multipleStatements: true
        });

        fs.mkdirSync(path, { recursive: true });
        let promises = [];
        for (let index = starting; index <= nb; index++) {
            const dbName = `wordpress_${fabrique}_${promo}_${index}`;
            promises.push(
                fs.copy('/wp-initial', path + '/wordpress' + index).then(() => {
                    fs.writeFile(path + '/wordpress' + index + '/wp-config.php', config.replace('::database::', dbName));
                    let currentSQL = `DROP DATABASE IF EXISTS ${dbName};
                CREATE DATABASE ${dbName};
                USE ${dbName};` + dbSQL.replace(/\/lyon\/test\/wordpress1/g, `/${fabrique}/${promo}/wordpress${index}`);
                    return connection.query(currentSQL).catch(err => console.error(err));
                }).catch(e => console.error(e))
            );

        }
        Promise.allSettled(promises).then(() => {

            connection.destroy();
            childProcess.exec('chown 1002:33 -R /var/www/html/' + fabrique + '/' + promo);
            childProcess.exec('chmod 775 -R /var/www/html/' + fabrique + '/' + promo);
            lastRefresh = 0;
        });

    }


    res.redirect('/');
});


app.listen(3000, () => {
    console.log('wordpress-maker listening on 3000');
});

